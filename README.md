# BugEye

BugEye is a family of unit testing frameworks for various languages.


## Incarnations of BugEye


### PHP

| Language | Incarnation                               | Stability  |
|:---------|:------------------------------------------|:----------:|
| 7.2+     | [BugEye TH](https://gitlab.com/bugeye/th) | alpha      |